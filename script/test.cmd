echo off

echo Test Script

rmdir /s /q dist
mkdir dist\docx

mkdir dist\pdf

echo "test content 1" > dist\docx\readme.txt
echo "test content 2" > dist\pdf\readme.txt

echo Test Ends here!